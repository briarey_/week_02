package ru.edu;

import java.util.Collections;
import java.util.List;

/**
 * Необходимо реализовать методы модификации
 * и доступа к хранимым приватным переменным.
 */
public class TextStatistics {

    /**
     * Всего слов.
     */
    private long wordsCount;

    /**
     * Всего символов.
     */
    private long charsCount;

    /**
     * Всего символов без пробелов.
     */
    private long charsCountWithoutSpaces;

    /**
     * Всего знаков препинания.
     */
    private long charsCountOnlyPunctuations;


    /**
     * Получение количества слов.
     *
     * @return значение
     */
    public long getWordsCount() {
        return wordsCount;
    }

    /**
     * Увеличение счетчика слов.
     *
     * @param value - количество слов
     */
    public void addWordsCount(final long value) {
        wordsCount += value;
    }

    /**
     * Получение количества символов.
     *
     * @return значение
     */
    public long getCharsCount() {
        return charsCount;
    }

    /**
     * Увеличение счетчика символов.
     *
     * @param value - количество символов
     */
    public void addCharsCount(final long value) {
        charsCount += value;
    }

    /**
     * Получение количества символов без пробелов.
     *
     * @return значение
     */
    public long getCharsCountWithoutSpaces() {
        return charsCountWithoutSpaces;
    }

    /**
     * Увеличение счетчика символов без пробелов.
     *
     * @param value - количество символов без пробелов
     */
    public void addCharsCountWithoutSpaces(final long value) {
        charsCountWithoutSpaces += value;
    }

    /**
     * Получение количества знаков препинания.
     *
     * @return значение
     */
    public long getCharsCountOnlyPunctuations() {
        return charsCountOnlyPunctuations;
    }

    /**
     * Увеличение счетчика знаков препинания.
     *
     * @param value - количество знаков препинания
     */
    public void addCharsCountOnlyPunctuations(final long value) {
        charsCountOnlyPunctuations += value;
    }

    /**
     * Задание со звездочкой.
     * Необходимо реализовать нахождение топ-10 слов.
     *
     * @return List из 10 популярных слов
     */
    public List<String> getTopWords() {
        return Collections.emptyList();
    }

    /**
     * Текстовое представление.
     *
     * @return текст
     */
    @Override
    public String toString() {
        return "TextStatistics{"
                + "wordsCount=" + wordsCount
                + ", charsCount=" + charsCount
                + ", charsCountWithoutSpaces=" + charsCountWithoutSpaces
                + ", charsCountOnlyPunctuations=" + charsCountOnlyPunctuations
                + '}';
    }
}
