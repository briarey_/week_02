package ru.edu;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MySourceReader implements SourceReader {

    /**
     * Целевой файл, из которого будет производиться чтение.
     */
    private File file;

    /**
     * Установка источника.
     * В реализации тут будет приходить путь до файла-источника
     *
     * @param source - адресс файла
     */
    @Override
    public void setup(final String source) {
        if (source == null) {
            throw new IllegalArgumentException("source == null");
        }
        file = new File(source);
        if (!file.exists()) {
            throw new IllegalArgumentException("source file is not exist");
        }
    }


    /**
     * Метод для анализа источника.
     *
     * @param analyzer - логика подсчета статистики
     * @return - рассчитанная статистика
     */
    @Override
    public TextStatistics readSource(final TextAnalyzer analyzer) {
        if (analyzer == null) {
            throw new IllegalArgumentException("analyzer == null");
        }

        try (
                FileReader fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr)
        ) {
            processFile(analyzer, br);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        return analyzer.getStatistic();
    }

    /**
     * Построчное чтение файла-источника.
     *
     * @param analyzer - логика подсчета статистики
     * @param br       - буфер обмена
     * @throws IOException - исключение чтения буффера
     */
    private void processFile(
            final TextAnalyzer analyzer,
            final BufferedReader br
    ) throws IOException {
        String line;

        line = br.readLine();
        if (line != null) {
            analyzer.analyze(line);
        }

        while ((line = br.readLine()) != null) {
            analyzer.analyze("\n" + line);
        }
    }

    //Старый метод реализации считывания
    //из вебинара для ознакомления.
    /*
    private void oldMethodReader(final TextAnalyzer analyzer){
        FileReader fr = null;
        BufferedReader br = null;
        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);

            String line;
            while ((line = br.readLine()) != null) {
                analyzer.analyze(line);
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            RuntimeException ext = null;
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    ext = new RuntimeException(ex);
                }
            }
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException ex) {
                    ext = new RuntimeException(ex);
                }
            }
            if(ext != null){
                throw ext;
            }
        }
    }*/
}
