package ru.edu;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public class MyStatisticReporter implements StatisticReporter {

    /**
     * Файл для парсинга.
     */
    private File file;

    /**
     * Конструктор класса, устанавливающий значение
     * используемого файла.
     *
     * @param filePath - путь к файлу
     */
    public MyStatisticReporter(final String filePath) {
        file = new File(filePath);
    }

    /**
     * Формирование отчета.
     *
     * @param statistics - данные статистики
     */
    @Override
    public void report(final TextStatistics statistics) {
        try (FileOutputStream os = new FileOutputStream(file);
             OutputStreamWriter sw = new OutputStreamWriter(
                     os,
                     StandardCharsets.UTF_8);
             PrintWriter writer = new PrintWriter(sw)) {

            writer.println("Words: "
                    + statistics.getWordsCount());
            writer.println("Chars: "
                    + statistics.getCharsCount());
            writer.println("NotSpace: "
                    + statistics.getCharsCountWithoutSpaces());
            writer.println("Punctuations: "
                    + statistics.getCharsCountOnlyPunctuations());

        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}
