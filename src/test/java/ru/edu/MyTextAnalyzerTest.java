package ru.edu;

import org.junit.Test;

import static org.junit.Assert.*;

public class MyTextAnalyzerTest {

    @Test
    public void analyze() {
        TextAnalyzer analyzer = new MyTextAnalyzer();

        analyzer.analyze("12345");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(1,statistics.getWordsCount());
        assertEquals(5,statistics.getCharsCount());
        assertEquals(5,statistics.getCharsCountWithoutSpaces());
        assertEquals(0,statistics.getCharsCountOnlyPunctuations());
    }

    @Test
    public void analyzeMultiLine() {
        TextAnalyzer analyzer = new MyTextAnalyzer();

        analyzer.analyze("12345");
        analyzer.analyze("\nasdfqwer, 234");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(3,statistics.getWordsCount());
        assertEquals(19,statistics.getCharsCount());
        assertEquals(18,statistics.getCharsCountWithoutSpaces());
        assertEquals(1,statistics.getCharsCountOnlyPunctuations());
    }
}