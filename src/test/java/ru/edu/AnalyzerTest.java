package ru.edu;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AnalyzerTest {

    /**
     * По умолчанию задачи запускаются с рабочей директорией в корне проекта
     */
    public static final String FILE_PATH = "./src/test/resources/input_text.txt";
    public static final int EXPECTED_WORDS = 535-20;
    public static final int EXPECTED_CHARS = 3556;
    public static final int EXPECTED_CHARS_NO_SPACES = EXPECTED_CHARS - 728 + 54;
    public static final int EXPECTED_CHARS_ONLY_PUNCT = 167;
    private final String RESULT_PATH = "./result.txt";

    /**
     * Нужно переключиться на вашу реализацию
     */
    private final TextAnalyzer analyzer = new MyTextAnalyzer();

    /**
     * Нужно переключиться на вашу реализацию
     */
    private final SourceReader reader = new MySourceReader();

    private final StatisticReporter reporter = new MyStatisticReporter(RESULT_PATH);


    @Test
    public void validation() {
        reader.setup(FILE_PATH);
        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);

        System.out.println("\n"+statistics);

        assertEquals(EXPECTED_WORDS, statistics.getWordsCount());
        assertEquals(EXPECTED_CHARS, statistics.getCharsCount());
        assertEquals(EXPECTED_CHARS_NO_SPACES, statistics.getCharsCountWithoutSpaces());
        assertEquals(EXPECTED_CHARS_ONLY_PUNCT, statistics.getCharsCountOnlyPunctuations());

        List<String> topWords = statistics.getTopWords();

        reporter.report(statistics);

        assertEquals(0, topWords.size());
    }
}