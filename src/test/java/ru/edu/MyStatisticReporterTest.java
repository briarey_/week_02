package ru.edu;

import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MyStatisticReporterTest {

    public static final String REPORTER_RESULT = "./target/reporter_result.txt";

    @Test
    public void report() {
        MyStatisticReporter reporter = new MyStatisticReporter(REPORTER_RESULT);

        TextStatistics statistics = new TextStatistics();

        statistics.addCharsCount(100);
        statistics.addWordsCount(25);
        statistics.addCharsCountWithoutSpaces(105);
        statistics.addCharsCountOnlyPunctuations(15);

        reporter.report(statistics);

        List<String> lines = readFile(REPORTER_RESULT);

        assertEquals(4, lines.size());
        assertEquals("Words: 25", lines.get(0));
        assertEquals("Chars: 100", lines.get(1));
        assertEquals("NotSpace: 105", lines.get(2));
        assertEquals("Punctuations: 15", lines.get(3));

    }

    private List<String> readFile(String resultTxt) {
        List<String> lines = new ArrayList<>();
        try (FileReader fr = new FileReader(new File(resultTxt));
             BufferedReader br = new BufferedReader(fr)) {
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}